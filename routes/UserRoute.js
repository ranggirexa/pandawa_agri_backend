require('dotenv').config()
const express = require( "express")
const UserController = require ("../controllers/UserController")
const auths = require('../controllers/auth')
const midAuth = require('../middleware/middleware')

const router = express.Router()

router.get('/users' ,UserController.getUsers)
// router.post('/users', UserController.createUser)
router.post("/logins", auths.login)
router.post("/users", auths.register)


// module.exports = router
module.exports = router