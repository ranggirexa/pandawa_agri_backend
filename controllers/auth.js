const bcrypt = require('bcrypt')
const {User} = require('../models')
const jwt = require('jsonwebtoken');

refreshTokens = []
  
function generateAccessToken(user) {

    return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, {expiresIn: '150000s'} )
}

exports.register = async (req, res) => {
const hashedPassword = await bcrypt.hash(req.body.password, 10)
    User.findAll({
        raw : true ,
        nest: true ,
        where:{
            username:req.body.username
        }
    })
    .then(resp => {
        if (resp.length == 0) {
            
            User.create({
                username:req.body.username,
                password:hashedPassword,
                role:req.body.role
                
            })
            .then(resp => {
                res.json({
                    status:201,
                    message:'success insert data',
                    data:[
                     resp
                    ]
                })
            })
            .catch(err => {
                res.json({
                    status:400,
                    message:'failed insert data'
                })
            })
        }else{
            res.status(400)
            res.json({
                message:'failed, username was taken',
            })
        }
    })
    .catch(err => {
        res.status(500)
        res.json({
            message:err,
        })
    })

}


exports.login = async (req, res) => {
    arr_user = []
    token = []
    

    User.findAll({
        raw : true ,
        nest: true ,
        where:{
            username:req.body.username
        }
    }).then(resp => {
        if (resp.length == 0) {
            res.status(404)
            return res.json({
                message:`username ${req.body.username} tidak ditemukan`,
            })
        } else{
            const username = {name: req.body.username}
            const user = {name: username}
            
            const accessToken = generateAccessToken(user)
            const refreshToken = jwt.sign(user, process.env.REFRESH_TOKEN_SECRET)
            // console.log(resp[0]);
    
            token.push(accessToken)
            refreshTokens.push(refreshToken)
            arr_user.push(resp[0]);
            try {
                if(bcrypt.compare(req.body.password, arr_user[0].password)) {
        
                    req.session.username=req.body.username
                    req.session.role=arr_user[0].role
                    req.session.user_id=arr_user[0].id
        
                    res.status(200)
                    res.json({
                        message:'success',
                        refresh_token:refreshTokens[0],
                        access_token:token[0],
                        response:[
                            resp
                        ]
                    })
                } else {
                    res.status(400)
                    res.json({
                        message:'login failed',
                        response: null
                    })
                }
            } catch(error) {
                console.error(error);
                res.status(404)
                res.json({
                    message:'something wrong',
                    response: null
                })
            }
        }

    }).catch(err => {
        console.log(err.message)
        res.status(404)
        res.json({
            message: err
        })
    })
   
};

exports.refresh_token = async (req, res) => {
    const refreshToken = req.body.token
    if (refreshToken == null) {
        res.status(401) 
        return res.json({
            message:'Unauthorized'
        })
    }
    
    if(!refreshTokens.includes(refreshToken)) {
        res.status(403)
        return res.json({
            message:'forbidden, wrong token'
        })
    }

    jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
        if (err) {
            res.status(403)
            return res.json({
                message:'forbidden, wrong token'
            })
        }
        const acccessToken = generateAccessToken({name :user.name})
        res.json({accessToken: acccessToken})
    })
}

exports.logout = (req, res) => {
    refreshTokens = refreshTokens.filter(token => token !== req.body.token)
       res.status(200)
       return res.json({
           message:'logout succesfully'
       })
   
}