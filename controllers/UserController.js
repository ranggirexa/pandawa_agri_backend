// const {User} = require ("../models/user")
const {User} = require('../models')


module.exports = {
    // getUsers:async(req, res) => {
        // try {
        //     const response = await User.findAll()
        //     res.status(200).json(response)
        // } catch (error) {
        //     console.log(error.message);
        // }
    // },
    getUsers: async (req, res) => {
        User.findAll({
            raw : true ,
            nest: true 
        })
        .then(resp => {
            res.json({
                status:200,
                message:'success retrive data',
                data:[
                   resp
                ]
            })
        })
        .catch(err => {
            res.status(500)
            res.json({
                message:err.message,
            })
        })
    },
    createUser:(req, res) =>{
        const {username, password, role} = req.body
         User.create({
            username:username,
            password:password,
            role:role
        })
        .then(resp => {
            res.json({
                status:201,
                message:"user created"
            })
        }).catch(err => {
            res.json({
                status:403,
                message:'failed insert data ',err
            })
        })

    },
}

