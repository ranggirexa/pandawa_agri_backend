'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Transactions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id_petani: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {     
          model: 'Users',
          key: 'id'
        }
      },
      berat: {
        type: Sequelize.INTEGER
      },
      id_harga: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {        
          model: 'Prices',
          key: 'id'
        }
      },
      user_id_admin: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {        
          model: 'Users',
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Transactions');
  }
};