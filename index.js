const express = require ("express")
const session = require('express-session')
const cors = require ("cors")
const UserRoute = require ("./routes/UserRoute.js")

const app = express()
app.use(session({
    resave: true,
    saveUninitialized: true,
    secret: "secret"
}))

app.use(cors())
app.use(express.json())
app.use(UserRoute)



app.listen(5000, ()=> console.log(`server up and run on port 5000`))